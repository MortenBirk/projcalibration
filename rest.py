import json
import web
from UnityHandler import UnityMapper
from printNatNet import demo_recv_data
from PointSort import sort

from LeastSquares import LeastSquare

urls = (
    '/points', 'Service',
    '/calib', 'CalibrateService',
    '/fake', 'FakeDataService',
    '/size', 'SetSizeService',
)
app = web.application(urls, globals())

class Service:
    def POST(self):
        print("Getting Points")
        data = json.loads(web.data())
        lsq.add2dPoints(data)
        data3D = demo_recv_data(1)
        if len(data3D) < 8 or len(data3D) > 8:
            print("too few or too many points")
            print(len(data3D))
            raise Exception('MOTIVE WRONG NUMBER OF POINTS')
        else:
            print("data from motive fine")
        data3D = sort(data3D, 0.1)
        lsq.add3dPoints(data3D)
        res = [data3D];
        return res


class CalibrateService:
    def POST(self):
        print("Calibrating")
        matrices = lsq.leastSquares()
        lsq.savePointsToFile()
        res = mapper.parse(matrices);
        return res

class SetSizeService:
    def POST(self):
        data = json.loads(web.data())
        print(data)
        lsq.setResolution(data["width"], data["height"])
        mapper.setResolution(data["width"], data["height"])
        return web.data()

class FakeDataService:
    def POST(self):
        data = None;
        data3D = None;
        with open('data2D.txt', 'r') as infile:
            data = json.load(infile)
        lsq.set2dPoints(data)
        with open('data3D.txt', 'r') as infile:
            data3D = json.load(infile)
        lsq.set3dPoints(data3D)
        return {result: "Perfect Loaded"}

lsq = LeastSquare()
mapper = UnityMapper()
#print(data3D)
if __name__ == "__main__":
    app.run()
