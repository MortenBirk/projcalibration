from __future__ import print_function
import optirx as rx
import sys
import json
from PointSort import sort

def demo_recv_data(max_count):

    version = (2, 7, 0, 0)

    ip_addr = None

    dsock = rx.mkdatasock(ip_addr)
    count = 0
    while count < max_count:
        data = dsock.recv(rx.MAX_PACKETSIZE)
        packet = rx.unpack(data, version=version)
        if type(packet) is rx.SenderData:
            version = packet.natnet_version
            print("NatNet version received:", version)
        if type(packet) in [rx.SenderData, rx.ModelDefs, rx.FrameOfData]:
            return packet.other_markers
        count += 1


if __name__ == "__main__":
    points = demo_recv_data(1)
    if len(points) < 8 or len(points) > 8:
        print("too few or too many points")
    else:
        points = sort(points, 0.1)
        print(json.dumps(points, indent=4))
