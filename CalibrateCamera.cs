﻿using UnityEngine;
using System.Collections;

public class CalibrateCamera : MonoBehaviour {
    public Camera camera;
    // Use this for initialization
    void Start() {
        ApplyCalibration();
    }

    // Update is called once per frame
    void Update() {

    }

    private void ApplyCalibration()
    {
        transform.position = new Vector3(-0.6489462357547796f, 2.5569944057620044f, 1.3974914683599702f);
        transform.eulerAngles = new Vector3(25.919248654180592f, 132.65310417933986f, 70.75658449782523f);
        float[,] lol = new float[,] { { 0.13077816159459477f, 0, 1.2829453022813933f, 0 }, { 0, -0.08085675019429639f, 1.3283390849040275f, 0 }, { 0, 0, -1.0002000200020003f, -0.020002000200020003f }, { 0, 0, -1, 0 } };


        var persp = new Matrix4x4();
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                persp[i, j] = lol[i,j];
            }
        }
        camera.projectionMatrix = persp;
        camera.transform.localScale = new Vector3(1, 1, -1);
        print(camera.transform.position);
        Debug.Log(camera.projectionMatrix);
    }
}
