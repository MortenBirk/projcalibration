import numpy as np
import scipy.optimize as opt
import cv2
import json
import math

class LeastSquare:

    def __init__(self):
        self.points2D = []
        self.points3D = []
        self.width = 0
        self.height = 0

    def setResolution(self, x, y):
        self.width = x
        self.height = y

    def add2dPoints(self, data):
        self.points2D = self.points2D + data
        print("Length of points2D")
        print(len(self.points2D))

    def add3dPoints(self, points):
        for i in range(0,len(points)):
            self.points3D.append({"x":points[i][0],"y":points[i][1],"z":points[i][2],"w":1})
        print("Length of points3D")
        print(len(self.points3D))

    def set2dPoints(self, data):
        self.points2D = data

    def set3dPoints(self, data):
        self.points3D = data

    def savePointsToFile(self):
        with open('data2D.txt', 'w') as outfile:
            json.dump(self.points2D, outfile)
        with open('data3D.txt', 'w') as outfile:
            json.dump(self.points3D, outfile)

    def createPMatrix(self):
        pMatrix = []
        for i in range(0, len(self.points2D)):
            lineX = np.array([self.points3D[i]["x"],
                self.points3D[i]["y"],
                self.points3D[i]["z"],
                self.points3D[i]["w"],
                0,
                0,
                0,
                0,
                -self.points2D[i]["x"] * self.points3D[i]["x"],
                -self.points2D[i]["x"] * self.points3D[i]["y"],
                -self.points2D[i]["x"] * self.points3D[i]["z"],
                -self.points2D[i]["x"] * self.points3D[i]["w"]])

            lineY = np.array([0,
                0,
                0,
                0,
                self.points3D[i]["x"],
                self.points3D[i]["y"],
                self.points3D[i]["z"],
                self.points3D[i]["w"],
                -self.points2D[i]["y"] * self.points3D[i]["x"],
                -self.points2D[i]["y"] * self.points3D[i]["y"],
                -self.points2D[i]["y"] * self.points3D[i]["z"],
                -self.points2D[i]["y"] * self.points3D[i]["w"]])
            if i == 0:
                pMatrix = lineX
            else:
                pMatrix = np.vstack((pMatrix,lineX))
            pMatrix = np.vstack((pMatrix,lineY))
        return pMatrix

    def useOpenCV(self):
        data = self.points2D
        self.opencvpoints2D = []
        for i in range(0,len(data)):
            self.opencvpoints2D.append([data[i]["x"], data[i]["y"]])
        self.opencvpoints2D = np.array(self.opencvpoints2D)
        data = self.points3D
        self. opencvpoints3D = []
        for i in range(0,len(data)):
            self.opencvpoints3D.append([data[i]["x"], data[i]["y"], data[i]["z"]]);
        self.opencvpoints3D = np.array(self.opencvpoints3D)

        intrinsics = self.createIntrinsicGuess()
        flags = cv2.CALIB_USE_INTRINSIC_GUESS
        dist_coefs = np.zeros(4)
        self.opencvpoints3D = self.opencvpoints3D.astype('float32')
        self.opencvpoints2D = self.opencvpoints2D.astype('float32')
        print("===========")
        print(self.opencvpoints3D)
        print(self.opencvpoints2D)
        print("===========")
        w = self.width
        h = self.height
        size = (w,h)
        dist_coefs = np.zeros(4,'float32')
        retval,camera_matrix,dist_coefs,rvecs,tvecs = cv2.calibrateCamera([self.opencvpoints3D],[self.opencvpoints2D],size,intrinsics,dist_coefs,flags=cv2.CALIB_USE_INTRINSIC_GUESS | cv2.CALIB_FIX_K1 | cv2.CALIB_FIX_K2 | cv2.CALIB_FIX_K3 | cv2.CALIB_FIX_K4 | cv2.CALIB_FIX_K5 | cv2.CALIB_ZERO_TANGENT_DIST)
        print(cv2.Rodrigues(np.array(rvecs))[0])
        return camera_matrix, rvecs, tvecs, rvecs

    def useOwnAlgorithm(self):
        pMatrix = self.createPMatrix();

        def f(x):
            y = np.dot(pMatrix, x)
            return np.dot(y, y)
        cons = ({'type': 'eq', 'fun': lambda x: math.sqrt(x.dot(x)) - 1})

        #DO THE INITIAL GUESS ON INTRINSIC AND EXTRINSIC
        kInit = np.array([[343, 0, 244],[0,340,229],[0,0,1]])
        rInit = np.array([[-0.9,0.013,-0.36],[-0.21,0.56,0.77],[0.21,0.82,-0.52]]) #TODO Is this correct?
        posInit = np.array([0.0066964173,0.1649601479,-2.3626]) #TODO Is this correct
        aBMatrix = np.c_[rInit, posInit]
        initialGuess = kInit.dot(aBMatrix)

        print(initialGuess)
        res = opt.minimize(f, np.array(initialGuess), method='SLSQP', constraints=cons, options={'disp': False}).x
        #res = opt.minimize(f, np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]), method='SLSQP', constraints=cons, options={'disp': False}).x
        print("")
        print("")
        print("Norm is")
        print(np.linalg.norm(res))
        print("")
        print("")
        res = np.reshape(res, (3,4))

        print("")
        print("")
        print("PerspProj Mat is")
        print(res)
        print("With norm")
        print(np.linalg.norm(res))
        print("")
        print("")

        b = res[:,3]
        a1 = res[:,0]
        a2 = res[:,1]
        a3 = res[:,2]
        p = 1/np.linalg.norm(a3)
        r3 = p*a3
        uo = (p*p)*a1.dot(a3)
        vo = (p*p)*a2.dot(a3)

        a1crossa3 = np.cross(a1,a3)
        a2crossa3 = np.cross(a2,a3)
        cosTheta = - a1crossa3.dot(a2crossa3) / np.linalg.norm(a1crossa3)*np.linalg.norm(a2crossa3)
        theta = math.acos(cosTheta) #THIS VALUE IS IN radians, NOT SURE IT SHOULD BE     #THIS MIGHT JUST HAVE TO BE math.py/2 which means expecting no angle, and this makes the matrix look like it shuold
        alpha = (p*p)*np.linalg.norm(a1crossa3)*math.sin(theta)
        beta = (p*p)*np.linalg.norm(a2crossa3)*math.sin(theta)
        r1 = (1/np.linalg.norm(a2crossa3))*a2crossa3
        r2 = np.cross(r3, r1)
        k = np.array([[alpha,0,uo],[0,beta,vo],[0,0,1]])
        t = p*np.linalg.inv(k).dot(b)
        print("k is")
        print(k)
        print("r1")
        print(r1)
        print("r2")
        print(r2)
        print("r3")
        print(r3)
        print("uo")
        print(uo)
        print("v0")
        print(vo)
        print(cosTheta)
        print("alpha")
        print(alpha)
        print("beta")
        print(beta)
        print("t")
        print(t)

        intrinsics, rotation, translation, rotX, rotY, rotZ, euler = cv2.decomposeProjectionMatrix(res)
        diff = 0
        for i in range(0,8):
            inter = np.dot(res, np.array([self.points3D[i]["x"], self.points3D[i]["y"], self.points3D[i]["z"], self.points3D[i]["w"],]))
            pointDiff = abs(abs(self.points2D[i]["x"]) - abs(inter[0]/inter[2])) + abs(abs(self.points2D[i]["y"]) - abs(inter[1]/inter[2]))
            diff = diff + pointDiff

        print("diff is")
        print(diff)

        return intrinsics, rotation, translation, euler


    def leastSquares(self):
        intrinsics, rotation, translation, euler = self.useOpenCV()
        print("")
        print("Inverse Intrinsics Matrix is")
        print(np.linalg.inv(intrinsics))
        print("")
        return {"intrinsics":intrinsics, "rotation":rotation, "translation":translation, "euler":euler, "motive":self.points3D}


    def createIntrinsicGuess(self):
        height = self.height
        width = self.width
        hfov = 91.2705674249382
        vfov = 59.8076333281726
        #hfov = 70.6 #For the kinect, this is not used right now
        #vfov = 60 #For the kinect, this is not used right now
        fx = (width / (2.0 * math.tan(math.radians(0.5 * hfov))))
        fy = (height / (2.0 * math.tan(math.radians(0.5 * vfov))))
        cy = height / 2.0
        cx = width / 2.0
        intrinsics = np.zeros((3,3))
        intrinsics[0, 0] = fx
        intrinsics[0, 2] = cx
        intrinsics[1, 1] = fy
        intrinsics[1, 2] = cy
        intrinsics[2, 2] = 1
        return intrinsics;
