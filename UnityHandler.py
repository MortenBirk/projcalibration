import numpy as np
import cv2
import json
import math

class UnityMapper:

    def __init__(self):
        self.width = 0
        self.height = 0

    def setResolution(self, x, y):
        self.width = x
        self.height = y

    def parse(self, matrices):
        #CvMat rotationInverse = GetRotationMatrixFromRotationVector(rotation).Transpose(); // transpose is same as inverse for rotation matrix
        #CvMat transFinal = (rotationInverse * -1) * translation.Transpose();
        #ApplyTranslationAndRotationToCamera(transFinal, RotationConversion.RotationMatrixToEulerZXY(rotationInverse));

        #According to http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
        focalX = matrices["intrinsics"][0][0]
        focalY = matrices["intrinsics"][1][1]
        principalX = matrices["intrinsics"][0][2]
        principalY = matrices["intrinsics"][1][2]
        projectionMatrix = self.deriveProjectionMatrix(focalX, focalY, principalX, principalY)
        extrinsics = self.deriveExtrinsicsOpenCV(matrices["euler"], matrices["translation"])
        #extrinsics = self.deriveExtrinsics(matrices["euler"], matrices["translation"])
        #res = {"intrinsics":matrices["intrinsics"].tolist(), "rotation":matrices["rotation"].tolist(), "translation":matrices["translation"].tolist(), "position":extrinsics["position"], "projectionMatrix":projectionMatrix.tolist(), "angles":extrinsics["angles"], "motive":matrices["motive"]}
        res = {"intrinsics":matrices["intrinsics"].tolist(), "rotation":matrices["rotation"][0].tolist(), "translation":matrices["translation"][0].tolist(), "position":extrinsics["position"], "projectionMatrix":projectionMatrix.tolist(), "angles":extrinsics["angles"], "motive":matrices["motive"]}

        return json.dumps(res)



    def deriveProjectionMatrix(self, fx, fy, cx, cy):

        w = self.width
        h = self.height
        nearDist = 0.01 #NOT SURE ABOUT THIS
        farDist = 100 #NOT SURE ABOUT THIS

        #FROM https://github.com/andrewmacquarrie/UnityProjectionMapping/blob/master/Assets/Calibration.cs#L96
        return self.makeFrustumMatrix(
            nearDist * (-cx) / fx, nearDist * (w - cx) / fx,
            nearDist * (cy) / fy, nearDist * (cy - h) / fy,
            nearDist, farDist)

    def deriveExtrinsicsOpenCV(self, rvec, translation):
        translation = translation[0]
        def RotationMatrixToEulerZXY(R):
            def wrap_angles(angle, lower, upper):
                window = 2 * math.pi
                if (window - (upper - lower)) > 1e-7:
                    print("FAAAAAAAAAAAAAIIIIIIIIIIIIL")
                while(True):
                    if angle > upper:
                        angle = angle - window
                    elif angle < lower:
                        angle = angle + window
                    else:
                        break;
                return angle;


            def RadianToDegree(angle):
                return angle * (180.0 / math.pi)
            EULER_EPSILON = 0.00005
            i = 2
            j = 0 # EULER_NEXT[2]
            k = 1 # EULER_NEXT[3]

            cos_beta = math.sqrt(math.pow(R[i, i], 2) + math.pow(R[j, i], 2))

            alpha = None
            beta = None
            gamma = None
            if cos_beta > EULER_EPSILON:
                alpha = math.atan2(R[k, j], R[k, k])
                beta = math.atan2(-R[k, i], cos_beta)
                gamma = math.atan2(R[j, i], R[i, i])
            else:
                alpha = math.atan2(-R[j, k], R[j, j])
                beta = math.atan2(-R[k, i], cos_beta)
                gamma = 0.0

            alpha = wrap_angles(alpha, 0.0, 2.0 * math.pi) #Z
            beta = wrap_angles(beta, 0.0, 2.0 * math.pi) #X
            gamma = wrap_angles(gamma, 0.0, 2.0 * math.pi) #Y

            # errr... 180 - Z result seems right. Why?
            return [RadianToDegree(beta), RadianToDegree(gamma), 180.0 - RadianToDegree(alpha)]

        #flip the z axis
        rotationMatrix = cv2.Rodrigues(rvec[0])[0] #Create matrix from vector

        rotationMatrix = rotationMatrix.T #Inverse of rotation vector
        #FLIP THE ENTRIES IN LAST ROW
        rotationMatrix[2,0] = -rotationMatrix[2,0]
        rotationMatrix[2,1] = -rotationMatrix[2,1]
        rotationMatrix[2,2] = -rotationMatrix[2,2]

        t = np.dot((rotationMatrix * -1),translation)
        r = RotationMatrixToEulerZXY(rotationMatrix)

        t = t.tolist()
        t[0] = t[0][0]
        t[1] = t[1][0]
        t[2] = t[2][0]

        return  {"position":[t[0], t[1], t[2]], "angles":[r[0],r[1],r[2]]}
        #Time to get rotation matrix back to a vector



    def deriveExtrinsics(self, euler, translation):
        tx = (translation[0] / translation[3])[0]
        ty = (translation[1] / translation[3])[0]
        tz = (translation[2] / translation[3])[0]
        tz = -tz #CHANGE FROM RHS -> LHS

        rx = euler[0][0]
        ry = euler[1][0]
        rz = euler[2][0]
        rz = -rz #CHANGE FROM RHS -> LHS TODO Does this work?


        return {"position":[tx, ty, tz], "angles":[rx,ry,rz]}
        #_mainCamera.transform.position = new Vector3((float)tx, (float)ty, (float)tz);
        #_mainCamera.transform.eulerAngles = new Vector3((float)r.X, (float)r.Y, (float)r.Z);

    def makeFrustumMatrix(self, left, right, bottom, top, zNear, zFar):
        #https://github.com/openframeworks/openFrameworks/blob/master/libs/openFrameworks/math/ofMatrix4x4.cpp
        #note transpose of ofMatrix4x4 wr.t OpenGL documentation, since the OSG use post multiplication rather than pre.
        #NB this has been transposed here from the original openframeworks code

        A = (right + left) / (right - left);
        B = (top + bottom) / (top - bottom);
        C = -(zFar + zNear) / (zFar - zNear);
        D = -2.0 * zFar * zNear / (zFar - zNear);

        persp = np.zeros((4, 4))
        persp[0, 0] = 2.0 * zNear / (right - left);
        persp[1, 1] = 2.0 * zNear / (top - bottom);
        persp[2, 0] = A;
        persp[2, 1] = B;
        persp[2, 2] = C;
        persp[2, 3] = -1.0;
        persp[3, 2] = D;

        rhsToLhs = np.zeros((4, 4))
        rhsToLhs[0, 0] = 1.0;
        rhsToLhs[1, 1] = -1.0; # Flip Y (RHS -> LHS)
        rhsToLhs[2, 2] = 1.0;
        rhsToLhs[3, 3] = 1.0;

        return np.dot(rhsToLhs, persp.T)
