var camera, scene, renderer, lineGeo, lineMat;
var meshes = [];
init();
animate();
function init() {
  $(window).keypress((e) => {
    var code = e.keyCode || e.which;
    switch(code) {
      case 122:
        console.log("zoom in");
        camera.position.z -= 0.1;
        break;
      case 120 :
        console.log("zoom out");
        camera.position.z += 0.1;
        break;
      default:
        break;
    }
  });
  camera = new THREE.PerspectiveCamera( 70, 400 / 400, 0.01, 100 );
  camera.position.z = -1.5;
  camera.position.x = -0.5;
  camera.position.y = 1;
  camera.lookAt( new THREE.Vector3(0, 0, 0) )
  scene = new THREE.Scene();
  var texture = new THREE.TextureLoader().load( 'crate.gif' );
  var geometry = new THREE.BoxBufferGeometry( 0.03, 0.03, 0.03 );
  lineMat = new THREE.LineBasicMaterial({color: "rgb(255,255,255)"});
  var planegeometry = new THREE.PlaneGeometry(3, 3);
  var plane = new THREE.Mesh( planegeometry, new THREE.MeshBasicMaterial( {color: "rgb(55,55,55)", side: THREE.DoubleSide} ) );
  scene.add( plane );
  plane.rotation.x = Math.PI / 2;
  plane.rotation.y = 0;
  plane.rotation.z = Math.PI / 2;
  plane.position.x = 0.5;
  plane.position.y = -0.5;
  plane.position.z = 0.5;
  lineGeo = new THREE.Geometry();
  for(var i = 0; i < 8; i++) {
    var m = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: "rgb("+ i*30 +", "+ i*30 +", "+ 255 + i*30 +")", wireframe: true } ) )
    meshes.push(m);
    scene.add(m);
    m.position.x = 0;
    m.position.y = 0;
    m.position.z = 0;

  }



  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( 400, 400 );
  $("#three-drawer").append( renderer.domElement );
  //
  window.addEventListener( 'resize', onWindowResize, false );
}
function onWindowResize() {
  camera.aspect = 400 / 400;
  camera.updateProjectionMatrix();
  renderer.setSize( 400, 400 );
}

function setNewPositionOfPoint(point, index) {
  meshes[index].position.x = point.x;
  meshes[index].position.y = point.y;
  meshes[index].position.z = point.z;
  lineGeo.vertices[index] = new THREE.Vector3(point.x, point.y, point.z);
  if(index == 7) {
    var line = new THREE.Line(lineGeo, lineMat);
    scene.add(line);
  }
}

function animate() {
  requestAnimationFrame( animate );
  renderer.render( scene, camera );
}
