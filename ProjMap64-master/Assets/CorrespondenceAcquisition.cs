﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO: refactor so _height - pos.y is abstracted away

public class CorrespondenceAcquisition : MonoBehaviour
{
    public Texture2D _crosshairTexture;
    public Camera _mainCamera;

    private Calibration _calibrator;
    private int _minNumberOfPoints = 8;
    private List<Vector3> _objectPositions = new List<Vector3>();
    private List<Vector3> _imagePositions = new List<Vector3>();
    private bool _occludeWorld;
    private int _width;
    private int _height;
    private double _reprojectionError;
    private int? _dragging;
    private bool _calibrating;

    private static int IMAGE_POINT_MARKER_SIZE = 5;
    private static int IMAGE_POINT_HIGHLIGHTED_SIZE = 10;
    private static string CALIB_SPHERE_TAG = "CalibrationSphere";

    void Start()
    {
        _occludeWorld = false;
        _reprojectionError = 0.0;
        _height = (int)Screen.height;
        _width = (int)Screen.width;
        _calibrator = new Calibration(_mainCamera);
        _calibrating = true;
    }

    void OnGUI()
    {
        if (!_calibrating)
            return;

        if (_occludeWorld)
            OccludeScreen();

        _imagePositions.ForEach(delegate(Vector3 position) { DrawCrosshairImage(position, IMAGE_POINT_MARKER_SIZE); });
        if (ImagePointHighlighted())
            DrawCrosshairImage(_imagePositions[ImagePointMouseHooveringOver().Value], IMAGE_POINT_HIGHLIGHTED_SIZE);

        GUI.Box(new Rect(10, 10, 150, 90), "# points: " + _imagePositions.Count.ToString() + "\nReProj error: " + string.Format("{0:f2}", _reprojectionError));
    }

    private void DrawCrosshairImage(Vector3 position, int imagePointMarkerWidth)
    {
        GUI.DrawTexture(new Rect(position.x - imagePointMarkerWidth, position.y - imagePointMarkerWidth, imagePointMarkerWidth * 2, imagePointMarkerWidth * 2), _crosshairTexture);
    }

    private void OccludeScreen()
    {
        Texture2D blackTexture = new Texture2D(1, 1);
        blackTexture.SetPixel(0, 0, Color.black);
        GUI.DrawTexture(new Rect(0, 0, _width, _height), blackTexture);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (Input.GetKeyDown(KeyCode.Space))
            ToggleCalibrating();

        if (Input.GetMouseButtonDown(0))
        {
            if (ImagePointHighlighted())
            {
                _dragging = ImagePointMouseHooveringOver();
            }
            else
            {
                if (_imagePositions.Count == _objectPositions.Count)
                {
                    // We have the same number of object and image positions, so we are starting a new correspondence. First is the object position
                    CaptureWorldPoint();
                }
                else
                {
                    // we already have an object position, now we collect the 2D correspondence
                    CaptureImagePoint();
                    TriggerCalibration();
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            _dragging = null;
            TriggerCalibration();
        }

        if (_dragging != null)
        {
            var pos = Input.mousePosition;
            pos.y = _height - pos.y;

            _imagePositions[_dragging.Value] = pos;
        }
    }

    private void ToggleCalibrating()
    {
        _calibrating = !_calibrating;

        foreach (GameObject sphere in GameObject.FindGameObjectsWithTag(CALIB_SPHERE_TAG))
        {
			var renderer = sphere.GetComponent<Renderer>();
            renderer.enabled = _calibrating;
        }
    }

    private bool ImagePointHighlighted()
    {
        return ImagePointMouseHooveringOver() != null;
    }

    private int? ImagePointMouseHooveringOver()
    {
        Vector3 pos = Input.mousePosition;
        int? imagePosMatch = null;
        for (int i = 0; i < _imagePositions.Count; i++)
        {
            Vector3 imagePos = _imagePositions[i];
            if (Mathf.Abs(imagePos.x - pos.x) + Mathf.Abs(imagePos.y - (_height - pos.y)) < 5)
            {
                imagePosMatch = i;
            }
        }
        return imagePosMatch;
    }

    private void CaptureWorldPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit3d;
        if (Physics.Raycast(ray, out hit3d))
        {
            //CreateSphereAt(hit3d.point);
            _objectPositions.Add(hit3d.point);
            _occludeWorld = true;
        }
    }

    private void CaptureImagePoint()
    {
        Vector3 pos = Input.mousePosition;

        pos.y = _height - pos.y; // note the screen pos starts bottom left. We want top left origin
		//Debug.Log("Pos is");
		//Debug.Log(pos);
        _imagePositions.Add(pos);
        _occludeWorld = false;
    }

    private static void CreateSphereAt(Vector3 point)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = point;
        sphere.tag = CALIB_SPHERE_TAG;
    }

    private void TriggerCalibration()
    {
        if (_imagePositions.Count < _minNumberOfPoints)
            return;
        if (_imagePositions.Count != _objectPositions.Count)
            return;

		//TODO I AM INJECTING MY OWN POINTS HERE BIRK
		_objectPositions = new List<Vector3>();
		_objectPositions.Add (new Vector3(0.04239906743168831f, 0.11452700197696686f, -0.0024161571636795998f));
		_objectPositions.Add (new Vector3(0.2752625346183777f, -0.05953150615096092f, 0.05176953226327896f));
		_objectPositions.Add (new Vector3(0.5057263374328613f, 0.10786058008670807f, 0.1592159867286682f));
		_objectPositions.Add (new Vector3(0.4448264241218567f, -0.07731262594461441f, 0.37483668327331543f));
		_objectPositions.Add (new Vector3(0.35421907901763916f, 0.08459221571683884f, 0.5832152962684631f));
		_objectPositions.Add (new Vector3(0.12206483632326126f, -0.09788501262664795f, 0.5163417458534241f));
		_objectPositions.Add (new Vector3(-0.11179336905479431f, 0.08027926087379456f, 0.4306020140647888f));
		_objectPositions.Add (new Vector3(-0.06123276799917221f, -0.08376754820346832f, 0.2562282085418701f));

		Debug.Log (_objectPositions);

		//this.GetComponent<Renderer>().enabled = false;
		this.transform.localScale = new Vector3(0,0,0);
        _reprojectionError = _calibrator.calibrateFromCorrespondences(_imagePositions, _objectPositions);
    }

    public List<Vector3> GetImagePoints()
    {
        return _imagePositions;
    }

    public List<Vector3> GetWorldPoints()
    {
        return _objectPositions;
    }

    public void ReplayRecordedPoints(List<Vector3> worldPoints, List<Vector3> imgPoints)
    {
        _imagePositions = imgPoints;
        _objectPositions = worldPoints;
        foreach (var point in worldPoints)
            CreateSphereAt(point);
        TriggerCalibration();
    }
}