﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BirkSetPos : MonoBehaviour {
	public int idx;
	// Use this for initialization
	void Start () {
		List<Vector3> _objectPositions = new List<Vector3>();
		_objectPositions.Add (new Vector3(0.04239906743168831f, 0.11452700197696686f, -0.0024161571636795998f));
		_objectPositions.Add (new Vector3(0.2752625346183777f, -0.05953150615096092f, 0.05176953226327896f));
		_objectPositions.Add (new Vector3(0.5057263374328613f, 0.10786058008670807f, 0.1592159867286682f));
		_objectPositions.Add (new Vector3(0.4448264241218567f, -0.07731262594461441f, 0.37483668327331543f));
		_objectPositions.Add (new Vector3(0.35421907901763916f, 0.08459221571683884f, 0.5832152962684631f));
		_objectPositions.Add (new Vector3(0.12206483632326126f, -0.09788501262664795f, 0.5163417458534241f));
		_objectPositions.Add (new Vector3(-0.11179336905479431f, 0.08027926087379456f, 0.4306020140647888f));
		_objectPositions.Add (new Vector3(-0.06123276799917221f, -0.08376754820346832f, 0.2562282085418701f));
		Vector3 p = _objectPositions [idx];
		this.transform.position = new Vector3(p.x, p.y, -p.z);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
