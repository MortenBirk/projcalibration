﻿using UnityEngine;
using System.Collections;

public class CalibrateCamera : MonoBehaviour {
    public Camera camera;
    // Use this for initialization
    void Start() {
        ApplyCalibration();
    }

    // Update is called once per frame
    void Update() {

    }

    private void ApplyCalibration()
    {
		transform.position = new Vector3(0.5876136824541793f,1.8009726830070978f,1.4685829930555832f);
		transform.eulerAngles = new Vector3(20.683311274529824f,202.20758155608178f,0.4049778088255209f);
		float[,] lol = new float[,] {{1.4750012890756217f,0,0.039884026831744135f,0},{0,2.7108459449983275f,-0.9426406521290372f,0},{0,0,-1.0002000200020003f,-0.020002000200020003f},{0,0,-1f,0}};


        var persp = new Matrix4x4();
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                persp[i, j] = lol[i,j];
            }
        }
        camera.projectionMatrix = persp;
        camera.transform.localScale = new Vector3(1, 1, -1);
        print(camera.transform.position);
        Debug.Log(camera.projectionMatrix);
    }
}
